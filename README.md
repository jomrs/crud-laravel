# CRUD em Laravel

## Tecnologias
 - PHP 7;
 - MySql 8;
 - Laravel;

Instruções para rodar o projeto laravel estão dentro do [README.md](./src/README.md) em `src`.


# Utilizando o docker como ambiente

## Criar as máquinas
``` console
$: sudo docker-compose up --build
```

## Criar o .env dentro de [src](./src):
```
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
MYSQL_VERSION=5.6
DB_DATABASE=db_laravel
DB_USERNAME=root
DB_PASSWORD=root
```

## Logar no container app e rodar o compose update e as migrations:
``` console
$: sudo docker sudo docker-compose exec db bash
#: composer update
#: php artisan config:cache
#: php artisan migrate
```