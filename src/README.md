<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## CRUD Para Gerenciar Passageiros de Onibus

Projeto de CRUD, onde é possivel cadastrar passageiros, visualizar, atualizar e excluir os mesmos.

---
### Necessário criar um arquivo .env com o conteúdo:
Gerar chave:
> php artisan key:generate
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=Alterar pela chave gerada
APP_DEBUG=true
APP_URL=http://localhost


DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db_laravel
DB_USERNAME=root
DB_PASSWORD=
```