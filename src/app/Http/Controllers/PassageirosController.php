<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Passageiro;
use Illuminate\Database\Eloquent\Collection;

class PassageirosController extends Controller
{
    //

    public function cadastrar() {
        return view('viajem.cadastrar');
    }

    public function store(Request $request) {
        Passageiro::create([
            'nome' => $request->nome,
            'cpf' => preg_replace('/\D/', '', $request->cpf),
            'celular' => preg_replace('/\D/', '', $request->celular),
            'endereco' => $request->endereco,
            'sexo' => $request->sexo,
            'idade' => $request->idade,
            'qtd_malas' => $request->qtd_malas
        ]);

        return redirect('/passageiros')->with('message', 'Passageiro Cadastrado com sucesso!');
    }

    public function obterTodos() {
        $passageiros = Passageiro::query()->orderBy('nome')->get();

        return view('viajem.passageiros', ['passageiros' => $passageiros]);

    }

    public function editar(Request $request, $id) {
        if ($request->isMethod('PUT')) {
            $this->atualizar($request, $id);
            return redirect('/passageiros')->with('message', 'Passageiro Atualizado com sucesso!');
        }
        $passageiro = Passageiro::findOrFail($id);
        return view('viajem.editar', ['passageiro' => $passageiro]);
    }

    public function atualizar(Request $request, $id) {
        $passageiro = Passageiro::findOrFail($id);
        $passageiro->update([
            'nome' => $request->nome,
            'cpf' => preg_replace('/\D/', '', $request->cpf),
            'celular' => preg_replace('/\D/', '', $request->celular),
            'endereco' => $request->endereco,
            'sexo' => $request->sexo,
            'idade' => $request->idade,
            'qtd_malas' => $request->qtd_malas,
        ]);
    }

    public function excluir($id) {
        $passageiro = Passageiro::findOrFail($id);
        $passageiro->delete();

        return redirect('/passageiros')->with('message', 'Passageiro Excluido com sucesso!');
    }
}
