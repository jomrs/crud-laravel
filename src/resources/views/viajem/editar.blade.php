<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Editar o registro do passageiro {{ $passageiro->nome }}</title>
</head>
<body>
    @include('viajem.template')
    <div class="section mx-auto">

        <div class="alert mx-auto alert-danger col-md-3" role="alert">
            Editando o passageiro <b>"{{ $passageiro->nome }}"</b>.
        </div>

        <div class="section col-sm-5 mx-auto">
            <form action="{{ route('editar_passageiro', ['id' => $passageiro->id ]) }}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                      <label for="validationTooltip01">Nome</label>
                      <input type="text" class="form-control" name="nome" value="{{ $passageiro->nome }}" placeholder="Nome Completo">
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="validationTooltip02">CPF</label>
                      <input type="text" class="form-control" name="cpf" value="{{ $passageiro->cpf }}" placeholder="11 Dígitos do CPF." maxlength="11">
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="validationTooltipUsername">Número de Celular</label>
                        <input type="text" class="form-control" name="celular" value="{{ $passageiro->celular }}" placeholder="Digite seu número para contato." maxlength="12">
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col-md-9 mb-4">
                      <label for="validationTooltip03">Endereço</label>
                      <input type="text" class="form-control" name="endereco" value="{{ $passageiro->endereco }}" placeholder="Seu endereço completo.">
                      
                    </div>
                    <div class="col-md-1 mb-3">
                        <label for="validationTooltip03">Sexo</label>
                        <input type="text" class="form-control" name="sexo" value="{{ $passageiro->sexo }}" placeholder="M/F" maxlength="1">
            
                    </div>
                    <div class="col-md-2 mb-1">
                      <label for="validationTooltip04">Idade</label>
                      <input type="text" class="form-control" name="idade" value="{{ $passageiro->idade }}" maxlength="2" pattern="[0-9]+" maxlength="2">
                      
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="validationTooltip05">Quantidade de Malas</label>
                      <input type="number" class="form-control" name="qtd_malas" value="{{ $passageiro->qtd_malas }}" placeholder="Quantidade de Baguagem.">
                      
                    </div>
                  </div>
                <button type="submit" class="btn btn-primary">Atualizar</button>
              </form>
        </div>
    </div>
  <script>
    $(document).ready(function() {
      $('[name="cpf"]').mask('000.000.000-00', {reverse: true});
      $('[name="celular"]').mask('(00) 0000-0000');
    });
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</body>
</html>