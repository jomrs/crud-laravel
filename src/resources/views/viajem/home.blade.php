<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gerenciamento de Passageiros de Viajens</title>
</head>
<body>
    @include('viajem.template')
    <div class="section mx-auto" style="text-align:center;">
        <h1>Gerenciando seus queridos passageiros.</h1>
        <hr>
        <img class="shadow" style="margin:0 auto; display: block; border-radius: 8px;" src="{{ asset('images/transporte.jpg') }}" alt="">
    </div>
</body>
</html>