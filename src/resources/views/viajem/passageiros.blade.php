<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Passageiros Cadastrados</title>
</head>
<body>
    @include('viajem.template')
    <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">CPF</th>
            <th scope="col">Celular</th>
            <th scope="col">Endereço</th>
            <th scope="col">Sexo</th>
            <th scope="col">Idade</th>
            <th scope="col">Quantidade de Malas</th>
            <th scope="col">Operações</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($passageiros as $passageiro)
          <tr>
            <th scope="row">{{ $passageiro->nome }}</th>
            <td> {{ $passageiro->cpf }} </td>
            <td> {{ $passageiro->celular }} </td>
            <td> {{ $passageiro->endereco }} </td>
            <td> {{ $passageiro->sexo }} </td>
            <td> {{ $passageiro->idade }} </td>
            <td> {{ $passageiro->qtd_malas }} </td>
            <td>
                <button class="btn-info btn" onclick="window.location.href='{{ route('editar_passageiro', ['id' => $passageiro->id] ) }}'">editar</button>
                <button class="btn btn-danger" onclick="window.location.href='/excluir/{{ $passageiro->id }}'">excluir</button>
            </td>
          </tr>
        </tbody>
        @endforeach
      </table>
    @if(session()->has('message'))
      <!--
        <div class="alert alert-success col-md-3 mx-auto">
            {{ session()->get('message') }}
        </div>
      -->
      <div style="position: absolute; bottom: 2%; right: 2%;">
        <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="true" data-delay='5000'>
          <div class="toast-header">
            <strong class="mr-auto">Aviso</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="toast-body">
            {{ session()->get('message') }}
          </div>
        </div>
      </div>
      <script>
        $(function() {
          $('.toast').toast('show');
        })
      </script>
      @endif
</body>
</html>