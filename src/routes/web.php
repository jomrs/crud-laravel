<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PassageirosController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('viajem.home');
    //return view('welcome');
});

Route::get('/cadastrar', [PassageirosController::class, 'cadastrar']);
Route::post('/cadastrar', [PassageirosController::class, 'store'])->name('cadastrar_passageiro');
Route::get('/passageiros', [PassageirosController::class, 'obterTodos']);
Route::match(['get', 'put', 'post'], '/editar/{id}', [PassageirosController::class, 'editar'])->name('editar_passageiro');
Route::get('/excluir/{id}', [PassageirosController::class, 'excluir']);